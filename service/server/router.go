package main

import (
	"net/http"

	"gitlab.com/mfrzky/fortest/handler"

	"github.com/gin-gonic/gin"
)

func router01() http.Handler {
	// router setup
	rtr := gin.New()

	// middleware
	rtr.Use(gin.Recovery())

	// route static
	rtr.Static("/static", "./static")

	// API V0
	API := rtr.Group("/api")
	{
		API.POST("/user/signup", handler.SignUp())
	}

	return rtr
}
