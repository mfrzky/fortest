package provider

type SetToken struct {
	Token    string
	URL      string
	Username string
	Password string
}

var Token *SetToken