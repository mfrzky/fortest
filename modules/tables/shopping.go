package table

import "time"

type Shopping struct {
	ID          uint   `gorm:"primary_key"`
	Name        string `gorm:"type:varchar(25)"`
	CreatedDate time.Time
}

func (Shopping) TableName() string {
	return "shopping"
}