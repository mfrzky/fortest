package table

type User struct {
	ID 		 uint   `gorm:"primary_key"`
	Username string `gorm:"type:varchar(25)"`
	Password string `gorm:"type:varchar(25)"`
	Email	 string `gorm:"type:varchar(25)"`
	Phone	 string `gorm:"type:varchar(25)"`
	Country  string `gorm:"type:varchar(25)"`
	City 	 string `gorm:"type:varchar(25)"`
	Postcode int
	Name 	 string `gorm:"type:varchar(25)"`
	Address  string `gorm:"type:varchar(25)"`
}

func (User) TableName() string {
	return "user"
}