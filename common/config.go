package common

import (
	"flag"
	"time"

	"github.com/jinzhu/configor"
	"gitlab.com/mfrzky/fortest/modules/api/provider"
	"gitlab.com/mfrzky/fortest/modules/tables"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type Application struct {
	DB     *gorm.DB
	Config *Config
	Log    *zap.Logger
}

type Config struct {
	ServiceName  string
	Debug        bool
	LogPath      string
	LogName      string
	HttpAddress  map[string]string `required:"true"`
	DBConfig     DBConfig
	TimeZone     string
	TimeLocation *time.Location
	Provider      provider.SetToken
}

var App Application

func SetupConfig() error {
	var (
		config     Config
		configFile string
		debug      bool
	)

	// variable flag
	flag.StringVar(&configFile, "config", "", "set config filename (default: empty)")
	flag.BoolVar(&debug, "debug", false, "set debug mode true/false (default: false)")
	flag.Parse()

	// set debug & api mode
	config.Debug = debug

	err := configor.New(&configor.Config{
		ErrorOnUnmatchedKeys: true,
		Debug:                debug,
		Verbose:              debug,
		AutoReload:           true,
		AutoReloadInterval:   time.Minute * 10,
	}).Load(&config, configFile)
	if err != nil {
		return err
	}

	// set time location
	config.TimeLocation, err = time.LoadLocation(config.TimeZone)
	if err != nil {
		return err
	}

	// setup database
	App.DB, err = DBConnect(config.DBConfig)
	if err != nil {
		return err
	}

	// migration/create table structure
	err = App.DB.AutoMigrate(
		table.User{},
		table.Shopping{},
	)
	if err != nil {
		return err
	}

	// assign config to global var
	App.Config = &config

	return nil
}
