package handler

import (
	"bytes"
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	core "gitlab.com/mfrzky/fortest/common"
	"gitlab.com/mfrzky/fortest/modules/api/provider"
	"go.uber.org/zap"
	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

type SignUpForm struct {
	User struct {
		Username   string `json:"username"`
		Email      string `json:"email"`
		EncrypPass string `json:"encrypted_password"`
		Phone	   string `json:"phone"`
		Address    string `json:"address"`
		City       string `json:"city"`
		Country    string `json:"country"`
		Name       string `json:"name"`
		PostCode   string `json:"postcode"`
	} `json:"user"`
	ctx		 ServiceContext
}

type SignUpResult struct {
	Email    	string `json:"email"`
	Token    	string `json:"token"`
	Username  	string `json"username"`
	Error	 	error
	SignUpForm  SignUpForm
	ctx 	 	ServiceContext
}

type ServiceContext struct {
	DB     *gorm.DB
	Logger *zap.Logger
}

func SignUp() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		result := Request(
			ServiceContext{
				DB:     core.App.DB,
			},
			SignUpForm{
				
			},
		)
		// result.SignUpForm.SaveUser()
		if result.Error != nil {
			Response(ctx, &SignUpResult{}, nil)
			return
		}
	}
}

func Request(ctx ServiceContext, params SignUpForm) SignUpResult {
	var (
		SU = &SignUpForm{}
		result = SignUpResult{
			ctx: ctx,
			Email: SU.User.Email,
			Token: "",
			Username: SU.User.Username,
			SignUpForm: params,
		}
		client = &http.Client{Timeout: time.Second * 15}
		errMsg string
	)

	payload, err := json.Marshal(result)
	if err != nil {
		errMsg = "[error] - can't marshal request body to json"
		core.App.Log.Warn(errMsg, zap.Error(err))
	}

	// prepare request
	req, err := http.NewRequest("POST", "", bytes.NewBuffer(payload))
	if err != nil {
		core.App.Log.Warn("[error] - unmarshal response body", zap.Error(err))
	}

	// set header
	req.Header.Set("Content-Type", "application/json")

	// do request
	resp, err := client.Do(req)
	if err != nil {
		errMsg = "[error] - http request"
		result.Error = err
		return result
	}
	defer resp.Body.Close()

	// read response body
	respbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		core.App.Log.Warn("[error] - unmarshal response body", zap.Error(err))
	}

	// parse response body
	if err = json.Unmarshal(respbody, &result); err != nil {
		core.App.Log.Warn("[error] - unmarshal response body", zap.Error(err))
	}

	return result
}

func (c *SignUpForm) SaveUser() error {

	user := map[string]interface{}{
		"username": 	 c.User.Username,
		"password":      c.User.EncrypPass,
		"email":         c.User.Email,
		"phone":		 c.User.Phone,
		"country":		 c.User.Country,
		"city":		     c.User.City,
		"postcode":		 c.User.PostCode,
		"name":		     c.User.Name,
		"address":		 c.User.Address,
	}

	if err := c.ctx.DB.Table("charges").Create(&user).Error; err != nil {
		return err
	}

	return nil
}

func Response(c *gin.Context, res *SignUpResult, ext interface{}) {
	var response = gin.H{
		"email":res.Email,
		"token":res.Token,
		"username":res.Username,
	}

	if ext != nil {
		switch data := ext.(type) {
		case map[string]interface{}:
			for key, val := range data {
				response[key] = val
			}
		case gin.H:
			for key, val := range data {
				response[key] = val
			}
		}
	}

	c.JSON(200, response)
	c.Set("user", response)
}

func GetToken() (string, error) {
	payload := url.Values{}
	payload.Set("grant_type", "client_credentials")

	b := make([]byte, 150)
    if _, err := rand.Read(b); err != nil {
        return "", err
    }

	req, err := http.NewRequest("POST", hex.EncodeToString(b), strings.NewReader(payload.Encode())) // URL-encoded payload
	if err != nil {
		return "", err
	}
	
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", provider.Token.Token))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	body, _ := ioutil.ReadAll(resp.Body)
	data := make(map[string]interface{})
	if err = json.Unmarshal(body, &data); err != nil {
		return "", err
	}

	return fmt.Sprint(data["access_token"]), nil
}